extends Node

export (PackedScene) var LocalVehicle
export (PackedScene) var LocalPed
var score

const DIFFICULTY_TIMINGS = {"default": 1.5, "easy": 1.2, "medium": 1.0, "hard": 0.8, "harder": 0.6, "hardest": 0.4, "extreme": 0.1}

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()

func game_over():
	get_tree().call_group("localVehicles", "queue_free")
	get_tree().call_group("localPedestrians", "queue_free")
	defaultDifficulty()
	$ScoreTimer.stop()
	$LocalVehicleTimer.stop()
	$LocalPedTimer.stop()
	$HUD.show_game_over()
	
func new_game():
	score = 0
	$StartTimer.start()
	$HUD.update_score(score)
	$HUD.show_message("Get Ready", 2)

func _on_StartTimer_timeout():
	$LocalVehicleTimer.start()
	$LocalPedTimer.start()
	$ScoreTimer.start()
	var player = get_node("Player")
	player.start($StartPosition.position)

func _on_Player_hit():
	game_over()

func _on_LocalVehicleTimer_timeout():
	# Choose a random location on Path2D.
	$LocalVehiclePath/LocalVehicleSpawnLoc.offset = -randi()
	# Create a LocalVehicle instance and add it to the scene.
	var localVehicle = LocalVehicle.instance()
	localVehicle.increaseMinMaxSpeeds(score)
	add_child(localVehicle)
	# Set the localVehicle's direction perpendicular to the path direction.
	var direction = $LocalVehiclePath/LocalVehicleSpawnLoc.rotation + PI / 2
	# Set the localVehicle's position to a random location.
	localVehicle.position = $LocalVehiclePath/LocalVehicleSpawnLoc.position
	# Set the velocity (speed & direction).
#	localVehicle.increaseMinMaxSpeeds(score)
#	localVehicle.linear_velocity = Vector2(rand_range(localVehicle.min_speed, localVehicle.max_speed), 0)
#	localVehicle.linear_velocity = localVehicle.linear_velocity.rotated(direction)


func _on_LocalPedTimer_timeout():
		# Choose a random location on Path2D.
	$LocalPedPath/LocalPedSpawnLoc.offset = randi()
	# Create a LocalPed instance and add it to the scene.
	var localPed = LocalPed.instance()
	add_child(localPed)
	# Set the localPed's direction perpendicular to the path direction.
	var direction = $LocalPedPath/LocalPedSpawnLoc.rotation + PI / 2
	# Set the localPed's position to a random location.
	localPed.position = $LocalPedPath/LocalPedSpawnLoc.position
	# Set the velocity (speed & direction).
	localPed.linear_velocity = Vector2(rand_range(localPed.min_speed, localPed.max_speed), 0)
	localPed.linear_velocity = localPed.linear_velocity.rotated(direction)

func _on_ScoreTimer_timeout():
	score += 1
	$HUD.update_score(score)
	scaleDifficulty(score)

func scaleDifficulty(score):
	if score > 60 and $LocalVehicleTimer.get_wait_time() != DIFFICULTY_TIMINGS.extreme:
		$LocalVehicleTimer.set_wait_time(DIFFICULTY_TIMINGS.extreme)
	elif score > 50 and $LocalVehicleTimer.get_wait_time() != DIFFICULTY_TIMINGS.hardest:
		$LocalVehicleTimer.set_wait_time(DIFFICULTY_TIMINGS.hardest)
	elif score > 40 and $LocalVehicleTimer.get_wait_time() != DIFFICULTY_TIMINGS.harder:
		$LocalVehicleTimer.set_wait_time(DIFFICULTY_TIMINGS.harder)
	elif score > 30 and $LocalVehicleTimer.get_wait_time() != DIFFICULTY_TIMINGS.hard:
		$LocalVehicleTimer.set_wait_time(DIFFICULTY_TIMINGS.hard)
	elif score > 20 and $LocalVehicleTimer.get_wait_time() != DIFFICULTY_TIMINGS.medium:
		$LocalVehicleTimer.set_wait_time(DIFFICULTY_TIMINGS.medium)
	elif score > 10 and $LocalVehicleTimer.get_wait_time() != DIFFICULTY_TIMINGS.easy:
		$LocalVehicleTimer.set_wait_time(DIFFICULTY_TIMINGS.easy)
	else:
		pass
		
func defaultDifficulty():
	$LocalVehicleTimer.set_wait_time(DIFFICULTY_TIMINGS.default)

