extends RigidBody2D

var min_speed = 10
var max_speed = 20

const DIFFICULTY_SPEEDS = {
	"default": {"min": 10, "max": 20}, 
	"easy": {"min": 12, "max": 25}, 
	"medium": {"min": 15, "max": 30}, 
	"hard": {"min": 17, "max": 35}, 
	"harder": {"min": 20, "max": 40}, 
	"hardest": {"min": 22, "max": 45}, 
	"extreme": {"min": 25, "max": 50}
}

var set_speed = 0
var rng = RandomNumberGenerator.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	rng.randomize()
	set_speed = rng.randf_range(min_speed, max_speed)

func _physics_process(delta):
	position += Vector2(-100 * set_speed * delta,0)

func increaseMinMaxSpeeds(score):
	if score > 60 and min_speed != DIFFICULTY_SPEEDS.extreme.min:
		min_speed = DIFFICULTY_SPEEDS.extreme.min
		max_speed = DIFFICULTY_SPEEDS.extreme.max
	elif score > 50 and min_speed != DIFFICULTY_SPEEDS.hardest.min:
		min_speed = DIFFICULTY_SPEEDS.hardest.min
		max_speed = DIFFICULTY_SPEEDS.hardest.max
	elif score > 40 and min_speed != DIFFICULTY_SPEEDS.harder.min:
		min_speed = DIFFICULTY_SPEEDS.harder.min
		max_speed = DIFFICULTY_SPEEDS.harder.max
	elif score > 30 and min_speed != DIFFICULTY_SPEEDS.hard.min:
		min_speed = DIFFICULTY_SPEEDS.hard.min
		max_speed = DIFFICULTY_SPEEDS.hard.max
	elif score > 20 and min_speed != DIFFICULTY_SPEEDS.medium.min:
		min_speed = DIFFICULTY_SPEEDS.medium.min
		max_speed = DIFFICULTY_SPEEDS.medium.max
	elif score > 10 and min_speed != DIFFICULTY_SPEEDS.easy.min:
		min_speed = DIFFICULTY_SPEEDS.easy.min
		max_speed = DIFFICULTY_SPEEDS.easy.max
	else:
		pass

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func _on_LocalVehicleArea2D_area_entered(area):
	print("local vehicle area hit by: ", area.name)
