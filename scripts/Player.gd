extends Area2D

signal hit

var NoNo = preload("res://sounds/moonmoonNoNo.mp3")
var copSiren = preload("res://sounds/sirenSound.mp3")

const MOVE_SPEED = 500
var screen_size  # Size of the game window.

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size
	hide() # hidden at the start of the game, unhide when game is ready.

func _process(delta):
	var motion = Vector2()
	
	if Input.is_action_pressed("up"):
		motion.y -= 1
	if Input.is_action_pressed("down"):
		motion.y += 1
	if Input.is_action_pressed("left"):
		motion.x -= 1
	if Input.is_action_pressed("right"):
		motion.x += 1
		
	if motion.length() > 0:
		motion = motion.normalized() * MOVE_SPEED
		
	position += motion * delta
	position.x = clamp(position.x, 0, screen_size.x)
	position.y = clamp(position.y, 0, screen_size.y)
	
func start(pos):
	position = pos
	show()
	$CollisionShape2D.disabled = false
	$CopSirenAudio.stream = copSiren
	$CopSirenAudio.play()
	

func _on_Player_body_entered(body):
	print("player hit by: ", body.name)
	if "LocalVehicle" in body.name or "TileMapForeground" in body.name:
		hide()  # Player disappears after being hit.
		emit_signal("hit")
		$CollisionShape2D.set_deferred("disabled", true)
		if $CopSirenAudio.is_playing():
			$CopSirenAudio.stop()
	
	if "LocalPedestrian" in body.name:
		print("play audio")
		$PedestrianAudio.stream = NoNo
		$PedestrianAudio.play()
		

