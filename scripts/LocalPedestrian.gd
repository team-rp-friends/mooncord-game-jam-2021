extends RigidBody2D

export var min_speed = 1
export var max_speed = 5

var wasHit = false

var set_speed = 0
var rng = RandomNumberGenerator.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	rng.randomize()
	set_speed = rng.randf_range(min_speed, max_speed)

func _physics_process(delta):
	if !wasHit:
		position += Vector2(-700 * delta, -50 * set_speed * delta)

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func _on_LocalPedestrianArea2D_area_entered(area):
#	print("pedestrian area2d hit by:", area.name)
#	print("area.global_position:", area.global_position)
	if "Player" in area.name:
		wasHit = true
		rng.randomize()
		apply_impulse(Vector2(0,0), Vector2(1000,rng.randf_range(-800, 800)))
		#	explode or spin out to destroy the object
	if "LocalVehicle" in area.name:
		wasHit = true
		rng.randomize()
		apply_impulse(Vector2(0,0), Vector2(-1000,rng.randf_range(-800, 800)))
		#	explode or spin out to destroy the object
