extends CanvasLayer

signal start_game

func _ready():
	print("ready")
	$ColorRect.hide()

func show_message(text, timeout):
	$Message.text = text
	$Message.show()
	$MessageTimer.set_wait_time(timeout)
	$MessageTimer.start()
	
func darkSoulsFadeout():
	# Show our fade out background
	$ColorRect.show()
	# fade in the death message and rectangle for color
	$AnimationPlayer.play("DeathFadeOut")

func show_game_over():
	darkSoulsFadeout()
	show_message("uhhhh 350 VCB", 10)
	# wait until the messagetimer has counted down
	yield($MessageTimer, "timeout")
	
	# set the text color back to normal
#	$Message.add_color_override("font_color", Color(255,255,255,255))
	$Message.set_modulate(Color(255,255,255,255))
	$ColorRect.hide()
	$Message.text = "10-80 Activating"
	$Message.show()
	# make a one shot timer and wait for it to finish
	yield(get_tree().create_timer(1), "timeout")
	$StartButton.show()

func update_score(score):
	$ScoreLabel.text = str(score)


func _on_MessageTimer_timeout():
	$Message.hide()


func _on_StartButton_pressed():
	$StartButton.hide()
	emit_signal("start_game")
