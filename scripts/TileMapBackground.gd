extends Node2D
#extends TileMap

const VELOCITY = -7
var texture_width = 1600 

func _process(delta):
	position.x += VELOCITY
	_attempt_reposition()

func _attempt_reposition():
	if position.x < -texture_width:
		position.x += 2 * texture_width
